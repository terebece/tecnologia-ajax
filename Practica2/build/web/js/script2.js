function validarForm(){
    var nombre = document.forms["registro"]["nombre"].value;
    var correo = document.forms["registro"]["correo_e"].value;
    var contrasenia_1 = document.forms["registro"]["contrasenia_1"].value;
    var contrasenia_2 = document.forms["registro"]["contrasenia_2"].value;
    var telefono = document.forms["registro"]["telefono"].value;

    if(nombre == null || nombre.length == 0 || /^\s+$/.test(nombre)){
        alert('El campo Nombre de usuario no debe ir vacío');
        return false;
    }

    if(!(/\S+@\S+\.\S+/.test(correo))){
        alert('Debe escribir un Correo electrónico válido');
        return false;
    }

    var espacios_1 = espaciosEnBlanco(contrasenia_1);
    var espacios_2 = espaciosEnBlanco(contrasenia_2);

   
    if (espacios_1 || espacios_2) {
        alert ("La contraseña no puede contener espacios en blanco");
        return false;
    }

    if (contrasenia_1 != contrasenia_2) {
        alert("Las contraseñas deben de coincidir");
        return false;
    }

    if(contrasenia_1.length < 8) {
        alert('La contraseña debe de tener al menos 8 carácteres');
        return false;
    }

    if( !(/^\d{10}$/.test(telefono)) ) {
        alert('El telefono debe de tener 10 números');
        return false;
      }  
}

function espaciosEnBlanco(c) {
    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < c.length)) {
        if (c.charAt(cont) == " ")
            espacios = true;
            cont++;
    }
    return espacios;
}

function mConfirmacion() {
    var mensaje = confirm("¿Seguro que quieres salir de esta página?");
    if (mensaje) {
        window.location.href = "../index.html";
    }
}