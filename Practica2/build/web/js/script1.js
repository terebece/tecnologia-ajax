function listarEstados() {
    var listaEstados, long, i;
    var estados = document.createElement("SELECT");
    estados.setAttribute("id", "estados");
    document.body.appendChild(estados);
    
    listaEstados = ["Aguascalientes", "Baja California", "Baja California Sur", "Campeche",
                    "Chihuahua", "Chiapas", "Coahuila de Zaragoza", "Colima", "Durango",            
                    "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "Estado de México",
                    "Michoacán de Ocampo", "Morelos", "Nayarit", "Nuevo León", "Oaxaca",
                    "Puebla", "Querétaro", "Quintana Roo", "San Luis Potosí","Sinaloa",
                    "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", 
                    "Zacatecas","Ciudad de México"];

    long = listaEstados.length;

    for (i = 0; i < long; i++){
        var e = document.createElement("option");
        var v = listaEstados[i];
        e.setAttribute("value", v);
        var t = document.createTextNode(v);
        e.appendChild(t);
        document.getElementById("estados").appendChild(e);
    }
}

var i = 0;
var materiales = ["Una goma.", "Tres libretas.", "Cuatro libros.", "Una mochila.", "Dos Lapices.", 
                  "Tres plumas."];
longM = materiales.length;

function materialesNed(){
    var materialesN = document.createElement("ul");
    materialesN.setAttribute("id", "materiales");
    document.body.appendChild(materialesN);

    while(i < longM){
        var l = document.createElement("li");
        l.setAttribute("value", materiales[i]);
        var t = document.createTextNode(materiales[i]);
        l.appendChild(t);
        document.getElementById("materiales").appendChild(l);
        i++;
    }
}

var num1 = 4;
var num2 = 13;

var num3 = num1 * num2;
var num4 = num3 % 2;

function esPar(){
    var nuevaP = document.createElement("p"); 
    var contenido = ""
    if (num4 == 0){
        var text = "El producto de " + num1 + " y " + num2 + " es par.";
        contenido = document.createTextNode(text); 
    } else {
        var text = "El producto de " + num1 + " y " + num2 + " no es par.";
        contenido = document.createTextNode(text);
    }
    nuevaP.appendChild(contenido); 
    var p = document.getElementById("p1"); 
    document.body.insertBefore(nuevaP, p);
}





