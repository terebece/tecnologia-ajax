<div>
	<h2> Eerie Forest </h2>
	<img src="imagenes/bosque.jpg" alt="Foto de un bosque">
	<p>Eerie Forest es una colección de ambientes sutiles capturados por la noche en bosques densos en las regiones de Transilvania y Moldavia</p>
	<p>Imagen de <a href="https://pixabay.com/photos/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1208296">Free-Photos</a> 
	en <a href="https://pixabay.com/es/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1208296">Pixabay</a></p>
</div>
