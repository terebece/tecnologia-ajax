function myMove() {
	var elem = document.getElementById("myAnimation");
	var pos = 0;
	var pos_aux = 350;
	var id = setInterval(frame, 10);
	function frame() {
		if (pos == 700) {
			clearInterval(id);
		} else if (pos < 350) {
			pos++;
			elem.style.top = pos + 'px';
			elem.style.left = pos + 'px';
		} else {
			pos++;
			pos_aux--;
			elem.style.top = pos_aux + 'px';
			elem.style.left = pos + 'px';
		}
	}
}

function loadDoc() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("fun1").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "data.txt", true);
	xhttp.send();
}

function loadDoc2(url, cFunction) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			cFunction(this);
	    }
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

function loadDoc3(url, cFunction) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			cFunction(this);
	    }
	};
	xhttp.open("GET", url, false);
	xhttp.send();
}

function function1(xhttp) {
	document.getElementById("fun2").innerHTML = xhttp.responseText;
}

function function2(xhttp) {
	document.getElementById("fun3").innerHTML = xhttp.responseText;
}

function function3(xhttp) {
	document.getElementById("fun4").innerHTML = xhttp.responseText;
}
