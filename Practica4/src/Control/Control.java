package Control;

import Beans.Usuario;
import Model.MiModelo;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Teresa Becerril
 */
@WebServlet(name = "Control", urlPatterns = {"/control"})
public class Control extends HttpServlet {
	private static final long serialVersionUID = 1L;
    Usuario usuario;
    MiModelo model;
    
    public void init() {
		String cadenaCon = getServletContext().getInitParameter("cadenaCon");
		String userName = getServletContext().getInitParameter("userName");
		String password = getServletContext().getInitParameter("password");
		try {
			usuario = new Usuario();
			model = new MiModelo(cadenaCon, userName, password);
		} catch (Exception e) {
			
		}
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Control() {
		super();
	}

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Práctica 4</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> Práctica 4</h1>");;
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Usuario</title>");
            out.println("<link rel='stylesheet' type='text/css' href='css/estilo.css'/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class = 'row'>");
            out.println("<div class = 'row-3'>");
            out.println("<h1>Usuario</h1>");
            usuario.setCorreoE(request.getParameter("email"));
            usuario.setContrasenia(request.getParameter("password"));
            out.println("<p> Email: " + usuario.getCorreoE() + "</p>");
            out.println("<p> Password: " + usuario.getContrasenia() + "</p>");
            out.println("<br><br>");
            if (model.validarDatos(usuario.getCorreoE(), usuario.getContrasenia())) {
            	out.println("<p> Acceso concedido!! </p>");
            } else {
            	out.println("<p> Acceso denegado :c </p>");
            }
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Usuario</title>");
            out.println("<link rel='stylesheet' type='text/css' href='css/estilo.css'/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class = 'row'>");
            out.println("<div class = 'row-3'>");
            out.println("<h1>Usuario</h1>");
            usuario.setCorreoE(request.getParameter("email"));
            usuario.setContrasenia(request.getParameter("password"));
            out.println("<p> Email: " + usuario.getCorreoE() + "</p>");
            out.println("<p> Password: " + usuario.getContrasenia() + "</p>");
            out.println("<br><br>");
            if (model.validarDatos(usuario.getCorreoE(), usuario.getContrasenia())) {
            	out.println("<p> Acceso concedido!! </p>");
            } else {
            	out.println("<p> Acceso denegado :c </p>");
            }
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}