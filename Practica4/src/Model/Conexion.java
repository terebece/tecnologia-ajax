package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexion {
	private Connection connection;
    private String cadenaCon;
    private String userName;
    private String password;
    
    public Conexion(String cadenaCon, String userName, String password) {
		this.cadenaCon = cadenaCon;
		this.userName = userName;
		this.password = password;
	}

    public void conectar() throws SQLException {
        if (connection == null || connection.isClosed()) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            connection = DriverManager.getConnection(
                                        cadenaCon, userName, password);
        }
    }
     
    public void desconectar() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }

	public Connection getConnection() {
		return connection;
	} 

}
