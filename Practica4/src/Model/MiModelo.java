package Model;

import Model.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MiModelo {
	private Conexion con;
	
	public MiModelo(String cadenaCon, String userName, String password) throws SQLException {
		System.out.println(cadenaCon);
		con = new Conexion(cadenaCon, userName, password);
	}
	
	public boolean validarDatos (String correo_e, String contrasenia) {
		boolean  edo = false;
		try {
			String query = "select * from usuario where correo_e = ? and contrasenia = ?";
			System.out.println("Conectando....");
			con.conectar();
			System.out.println("Conexion establecida!!");
			Connection connection = con.getConnection();
			System.out.println("Conexion obtenida ....");
			PreparedStatement statement = connection.prepareStatement(query);
			System.out.println("Creando query ...");
			statement.setString(1, correo_e);
			statement.setString(2, contrasenia);
			System.out.println("Modificando query ...");
			ResultSet res = statement.executeQuery();
			edo = res.next();
			res.close();
			System.out.println("Query ejecutada...");
			con.desconectar();
			System.out.println("Desconectar ...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return edo; 
	}
}
