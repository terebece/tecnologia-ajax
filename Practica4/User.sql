CREATE DATABASE IF NOT EXISTS Usuarios;
USE Usuarios;

DROP TABLE IF EXISTS usuario;
CREATE TABLE usuario (
	correo_e varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    apellido_p varchar(50) NOT NULL,
    apellido_m varchar(50) NOT NULL,
    contrasenia varchar(40) NOT NULL,
    telefono varchar(10) NOT NULL,
    PRIMARY KEY (correo_e),
    UNIQUE(correo_e)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO usuario(correo_e, nombre, apellido_p, apellido_m, contrasenia, telefono) VALUES ("terebt@gmail.com", "Teresa", "Becerril", "Torres", "tbt123", "1234567891");
INSERT INTO usuario(correo_e, nombre, apellido_p, apellido_m, contrasenia, telefono) VALUES ("cbt@gmail.com", "Carmen", "Becerril", "Torres", "cbt1sdfsdf23", "1234567892");
INSERT INTO usuario(correo_e, nombre, apellido_p, apellido_m, contrasenia, telefono) VALUES ("carobt12@gmail.com", "Carolina", "Becerril", "Torres", "caroBT1234", "1234567893");

SELECT * FROM usuario;
SELECT * FROM usuario where correo_e = "terebt@gmail.com" and contrasenia = "tbt123";