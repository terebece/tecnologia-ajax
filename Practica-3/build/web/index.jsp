<%-- 
    Document   : index.jsp
    Created on : 19/10/2020, 09:43:56 AM
    Author     : Karme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import= "java.util.Date"%>
<%@page  import= "java.util.ArrayList"%>
<%@page  import= "java.util.List"%>
<%@page  import= "java.util.Random"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type ="text/css">
            <%@include file="css/estilo1.css"%>
        </style>
        <title>Práctica 3</title>
    </head>
    <body>
        <h1>Práctica 3</h1>
        <h2> Java Server Page</h2>
        <% Date date = new Date();
           Random r = new Random();
           int valorDado = r.nextInt(1000);
           List<String> atracciones = new ArrayList<String>();
           atracciones.add("The Blue Lagoon, Islandia");
           atracciones.add("Machu Picchu, Perú");
           atracciones.add("Stonehenge, Reino Unido");
           atracciones.add("Cristo Redentor, Brasil");
           atracciones.add("Museo del Louvre, París");
           atracciones.add("Acrópolis de Atenas, Grecia");
           atracciones.add("Torre Eiffel, Francia");
           atracciones.add("La Gran Muralla China");
           atracciones.add("La Gran Pirámide de Giza, Egipto");
           atracciones.add("Cataratas del Niágara, EE. UU./ Canadá");      
           int num = r.nextInt(10);
           String atraccion = atracciones.get(num);
           String[] animalesArtico = {"oso polar", "foca pia", "ballena jorobada", 
                                       "zorro ártico", "pingüino", "beluga", 
                                       "lobo ártico", "liebre ártica", "búho nival",
                                       "elefante marino"};
           
           String[] animales = {"oso polar", "león", "delfín", 
                                "zorro ártico", "pingüino", 
                                "elefante marino", "hipopótamo", "jaguar",
                                "perezoso", "koala"};
           int n = 0;
           String animal = animales[num]; 
           Boolean b = false;  
           while(n < animalesArtico.length){
             if (animal.equals(animalesArtico[n])){
                b = true;
             }
             n++;
           }
        %>
        <%= "<p> Fecha: " + date + "</p>"%>      
        <%= "<p> Número aleatorio: " + valorDado + "</p>"%>
        <p> La atracción turística más visitada es: <%= atraccion%> </p>
        <p> Atracciones turísticas famosas del mundo: </p>
        <% 
           for (int i = 0; i <= atracciones.size() - 1; i++) {
               int ind = i + 1;
        %>
        <p><%= ind + ". " + atracciones.get(i)%></p>
        <%}
          if (b) {
        %>
        <p> El <%=animal%> es un animal del Ártico</p>
        <%
          } else {
        %>
        <p> El <%=animal%> no es un animal del Ártico</p>
        <%
          }
        %>

        <h2>Servlets</h2>
        <form name="inputGet" action="/Practica-3/control" method="get" id="FormGet">
            <fieldset>
                <legend>Datos de la reserva - Get</legend>
                    <p>
                        Nombre
                        <input class="t-input"  type="text" name="nombre" required>
                    </p>
                    <p>
                        Apellidos
                        <input class="t-input" type="text" name="apellidos" required> 
                    </p>
                    <p>
                        Correo electrónico
                        <input class="t-input" type="email" name="correo_e" required>
                    </p>
                    <p>
                        Teléfono
                        <input class="t-input" type="text" name="telefono" 
                               minlength ="10" maxlength="10"required>
                    </p>
                    <p>
                        Fecha
                        <input class="t-input"  type="date" id="fecha_get" name="fecha"
                                value="2020-10-18" min="2020-10-01" max="2021-12-31" required>
                    </p>
                    <p>
                        Hora
                        <input class="t-input" type="time" name="hora" value="10:00:00" 
                               max="22:00:00" min="10:00:00" step="1" required>
                    </p>
                    <p>
                        Número de personas<br>
                        <input  type="radio" name="num_p" value="1 - 2" required> 1 - 2 <br>
                        <input  type="radio" name="num_p" value="3 - 4" required> 3 - 4 <br>
                        <input  type="radio" name="num_p" value="4 - 5" required> 5 - 6 <br>
                        <input  type="radio" name="num_p" value="7 - 8" required > 7 - 8 <br>
                        <input  type="radio" name="num_p" value="9 o mas" required> 9 o mas
                    </p>
                    <button class="button1" type="submit">Enviar</button>
            </fieldset>
        </form>
        <br><br>
        <form name="inputPost" action="/Practica-3/control" method="post" id="FormPost">
            <fieldset>
                <legend>Datos de la reserva - Post</legend>
                    <p>
                        Nombre
                        <input class="t-input" type="text" name="nombre" required>
                    </p>
                    <p>
                        Apellidos
                        <input class="t-input" type="text" name="apellidos" required>
                    </p>
                    <p>
                        Correo electrónico
                        <input class="t-input" type="email" name="correo_e" required>
                    </p>
                    <p>
                        Teléfono
                        <input class="t-input" type="text" name="telefono" 
                               minlength ="10" maxlength="10" required>
                    </p>
                    <p>
                        Fecha
                        <input class="t-input"  type="date" id="fecha_post" name="fecha"
                                value="2020-10-18" min="2020-10-01" max="2021-12-31" required>
                    </p>
                    <p>
                        Hora
                        <input class="t-input" type="time" name="hora" value="10:00:00" 
                               max="22:00:00" min="10:00:00" step="1" required>
                    </p>
                    <p>
                        Número de personas<br>
                        <input  type="radio" name="num_p" value="1 - 2" required> 1 - 2 <br>
                        <input  type="radio" name="num_p" value="3 - 4" required> 3 - 4 <br>
                        <input  type="radio" name="num_p" value="4 - 5" required> 5 - 6 <br>
                        <input  type="radio" name="num_p" value="7 - 8" required> 7 - 8 <br>
                        <input  type="radio" name="num_p" value="9 o mas" required> 9 o mas
                    </p>
                    <button class="button1" type="submit">Enviar</button>
            </fieldset>
        </form>
    </body>
</html>
