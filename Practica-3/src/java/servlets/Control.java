/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Beans.Reserva;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Teresa Becerril
 */
@WebServlet(name = "Control", urlPatterns = {"/control"})
public class Control extends HttpServlet {
    Reserva reserva;
    
    public Control (){
        reserva = new Reserva();
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Práctica 3</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> Práctica 3 </h1>");;
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Get</title>");
            out.println("<link rel='stylesheet' type='text/css' href='css/estilo1.css'/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class = 'row'>");
            out.println("<div class = 'row-1'>");
            out.println("<h1>Método Get</h1>");
            reserva.setNombre(request.getParameter("nombre"));
            reserva.setApellidos(request.getParameter("apellidos"));
            reserva.setCorreoE(request.getParameter("correo_e"));
            reserva.setTelefono(request.getParameter("telefono"));
            reserva.setFecha(request.getParameter("fecha"));
            reserva.setHora(request.getParameter("hora"));
            reserva.setNumP(request.getParameter("num_p"));
            out.println("<p> Nombre: " + reserva.getNombre() + "</p>");
            out.println("<p> Apellidos: " + reserva.getApellidos() + "</p>");
            out.println("<p> Correo electrónico: " + reserva.getCorreoE() + "</p>");
            out.println("<p> Teléfono: " + reserva.getTelefono() + "</p>");
            out.println("<p> Fecha: " + reserva.getFecha() + "</p>");
            out.println("<p> Hora: " + reserva.getHora() + "</p>");
            out.println("<p> Número de personas: " + reserva.getNumP() + "</p>");
            out.println("<br><br>");
            out.println("<a href = '/Practica-3'>Volver</a>");
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Post</title>");
            out.println("<link rel='stylesheet' type='text/css' href='css/estilo1.css'/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class = 'row'>");
            out.println("<div class = 'row-1'>");
            out.println("<h1>Método Post</h1>");
            reserva.setNombre(request.getParameter("nombre"));
            reserva.setApellidos(request.getParameter("apellidos"));
            reserva.setCorreoE(request.getParameter("correo_e"));
            reserva.setTelefono(request.getParameter("telefono"));
            reserva.setFecha(request.getParameter("fecha"));
            reserva.setHora(request.getParameter("hora"));
            reserva.setNumP(request.getParameter("num_p"));
            out.println("<p> Nombre: " + reserva.getNombre() + "</p>");
            out.println("<p> Apellidos: " + reserva.getApellidos() + "</p>");
            out.println("<p> Correo electrónico: " + reserva.getCorreoE() + "</p>");
            out.println("<p> Teléfono: " + reserva.getTelefono() + "</p>");
            out.println("<p> Fecha: " + reserva.getFecha() + "</p>");
            out.println("<p> Hora: " + reserva.getHora() + "</p>");
            out.println("<p> Número de personas: " + reserva.getNumP() + "</p>");
            out.println("<br><br>");
            out.println("<a href = '/Practica-3'>Volver</a>");
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
