/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;


/**
 *
 * @author Teresa Becerril Torres
 */
public class Reserva {
    String nombre;
    String apellidos;
    String correo_e;
    String telefono;
    String fecha;
    String hora;
    String num_p;
    
    public String getNombre(){
        return nombre;
    }
    
    public void setNombre(String nombre){
        this.nombre  = nombre;
    }
    
    public String getApellidos(){
        return apellidos;
    }
    
    public void setApellidos(String apellidos){
        this.apellidos  = apellidos;
    }
    
    public String getCorreoE(){
        return correo_e;
    }
    
    public void setCorreoE(String correo_e){
        this.correo_e  =  correo_e;
    }
    
    public String getTelefono(){
        return telefono;
    }
    
    public void setTelefono(String telefono){
        this.telefono  =  telefono;
    }
    
    public String getFecha(){
        return fecha;
    }
    
    public void setFecha(String fecha){
        this.fecha  =  fecha;
    }
    
    public String getHora(){
        return hora;
    }
    
    public void setHora(String hora){
        this.hora  =  hora;
    }
    
    public String getNumP(){
        return num_p;
    }
    
    public void setNumP(String num_p){
        this.num_p  = num_p;
    }
}
